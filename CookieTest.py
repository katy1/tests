import unittest
from selenium import webdriver
from helper import BaseMethod
from pages.PageAuth import PageAuth
from pages.PageMain import PageMain


class TestCase(unittest.TestCase):
    def setUp(self):
        self.page_main = PageMain()
        self.page_auth = PageAuth()
        self.driver = webdriver.Firefox()
        self.driver.get("http://www.way2automation.com/")

    def tearDown(self):
        self.driver.quit()

    def test_cookie(
            self):
        driver = self.driver
        self.page_main.click_button_login(driver)
        driver.switch_to_window(driver.window_handles[-1])
        self.page_auth.set_value_login(driver, 'katy_gubanova@mail.ru')
        self.page_auth.set_value_password(driver, '123456')
        self.page_auth.click_button_auth(driver)
        BaseMethod.receiving_cookie(driver)
        driver.delete_all_cookies()
        driver.refresh()
        BaseMethod.add_cookie(driver)
        driver.refresh()


if __name__ == '__main__':
    unittest.main()
