from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class PageMain(object):
    _locator = dict(element_button="//div[@class='btn-group margin_top_53']"
                                   "//a[@class='btn btn-primary']")

    def click_button_login(self, driver):
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((
            By.XPATH, self._locator['element_button'])))
        driver.find_element_by_xpath(self._locator['element_button']).click()
