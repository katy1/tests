from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class PageAuth(object):
    _locators = {
        'element_login': "user_email",
        'element_pass': "user_password",
        'element_sign': "//*[@class='btn btn-primary btn-md login-button']",
    }

    def click_button_auth(self, driver):
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((
            By.XPATH, self._locators['element_sign'])))
        return driver.find_element_by_xpath(
            self._locators['element_sign']).click()

    @staticmethod
    def find_element(driver, path_element):
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((
            By.ID, path_element)))
        return driver.find_element_by_id(path_element)

    def set_value_login(self, driver, value):
        self.find_element(
            driver, self._locators['element_login']).send_keys(value)

    def set_value_password(self, driver, value):
        self.find_element(
            driver, self._locators['element_pass']).send_keys(value)
