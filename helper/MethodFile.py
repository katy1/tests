import json


def save_cookies(cookies, file_name):
    with open(file_name, 'w') as outfile:
        json.dump(cookies, outfile)


def get_cookies(file_name):
    cookies = []
    with open(file_name, 'r') as file:
        cookies = json.load(file)
    return cookies
