import MethodFile


def receiving_cookie(driver):
    all_cookies = driver.get_cookies()
    cookies = []
    for cookie in all_cookies:
        cookie = {'name': cookie['name'], 'value': cookie['value'],
                  "domain": cookie['domain']}
        cookies += [cookie]
        MethodFile.save_cookies(cookies, "temp")


def add_cookie(driver):
    cookies = MethodFile.get_cookies("temp")
    for cookie in cookies:
        driver.add_cookie({'name': cookie['name'], 'value': cookie['value'],
                           "domain": cookie['domain']})
